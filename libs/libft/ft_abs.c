/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 18:53:00 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/12 19:49:35 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long long	ft_abs(long long nb)
{
	if (nb < 0)
		return (-nb);
	return (nb);
}
