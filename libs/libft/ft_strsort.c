/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 17:58:09 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/12 18:48:25 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	ft_strsort(char **str, int strnb)
{
	size_t	i;
	size_t	j;
	char	*buf;

	i = 0;
	j = strnb - 1;
	if (!str || !(*str))
		return ;
	while (j)
	{
		while (i < j && str[i + 1])
		{
			if (ft_strcmp(str[i], str[i + 1]) > 0)
			{
				buf = str[i];
				str[i] = str[i + 1];
				str[i + 1] = buf;
			}
			i++;
		}
		i = 0;
		j--;
	}
}
