/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 12:23:49 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/11 17:48:31 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

static int	ft_strendindex(int nb)
{
	int e;

	e = 0;
	if (nb < 0)
		e++;
	while ((nb = nb / 10))
		e++;
	return (e);
}

char		*ft_itoa(int n)
{
	size_t	i;
	char	*p;

	i = ft_strendindex(n);
	if (!(p = ft_strnew(i + 1)))
		return (NULL);
	if (n == -2147483648)
	{
		p[0] = '-';
		p[i--] = '8';
		n = (n / 10);
	}
	if (n < 0)
	{
		p[0] = '-';
		n = -n;
	}
	while (n >= 10)
	{
		p[i--] = (char)((n % 10) + '0');
		n /= 10;
	}
	p[i] = n + '0';
	return (p);
}
