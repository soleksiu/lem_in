/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 19:38:19 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/09 21:32:10 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strnstr(const char *hayst, const char *ndl, size_t len)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (*ndl == '\0')
		return ((char *)hayst);
	while (hayst[i] && i + ft_strlen(ndl) <= len && len)
	{
		j = 0;
		while (ndl[j] == hayst[i + j])
		{
			if (ndl[j + 1] == '\0')
				return ((char *)hayst + i);
			j++;
		}
		i++;
	}
	return (NULL);
}
