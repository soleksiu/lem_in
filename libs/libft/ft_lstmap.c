/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 18:46:53 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/07 20:43:42 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*pnew;
	t_list	*begin_new;

	if (!lst || !f)
		return (NULL);
	begin_new = f(lst);
	pnew = begin_new;
	while (lst->next)
	{
		lst = lst->next;
		if (!(pnew->next = f(lst)))
		{
			free(pnew->next);
			return (NULL);
		}
		pnew = pnew->next;
	}
	return (begin_new);
}
