/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_inlist.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 18:39:28 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/06 18:39:29 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_lst_inlist(t_list **alst, void *to_find)
{
	t_list	*lst_pt;

	if (!alst || !*alst || !to_find)
		return (0);
	lst_pt = *alst;
	while (lst_pt->next)
	{
		if (lst_pt->content == to_find)
			return (1);
		lst_pt = lst_pt->next;
	}
	return (0);
}
