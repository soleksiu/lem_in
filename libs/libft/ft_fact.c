/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fact.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 21:13:30 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/12 21:27:54 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

size_t		ft_fact(int nb)
{
	size_t	fact;

	fact = 1;
	if (nb < 0 || nb > 20)
	{
		ft_putendl("ft_fact: Incorrect input");
		return (0);
	}
	while (nb)
		fact = fact * nb--;
	return (fact);
}
