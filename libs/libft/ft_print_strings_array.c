/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_strings_array.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 21:56:49 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/04 21:56:50 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_print_strings_array(const char **s)
{
	unsigned int	i;

	i = 0;
	if (!s || !*s)
		return ;
	while (s[i])
		s[i + 1] == NULL ? ft_printf("%s", s[i++]) : ft_printf("%s ", s[i++]);
	ft_printf("\n");
}
