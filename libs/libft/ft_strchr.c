/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 21:12:46 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/09 19:22:28 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char *p;

	p = (char *)s;
	if (c == 0 && *s)
		return (p + (ft_strlen(s)));
	while (*p)
	{
		if (*p == c)
			return (p);
		p++;
	}
	return (NULL);
}
