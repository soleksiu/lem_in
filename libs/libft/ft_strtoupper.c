/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 17:36:15 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/09 17:36:16 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtoupper(char *str)
{
	char	*res;
	size_t	i;

	if (!str)
		return (NULL);
	res = ft_strdup(str);
	i = 0;
	while (res[i])
	{
		res[i] = ft_toupper(res[i]);
		i++;
	}
	return (res);
}
