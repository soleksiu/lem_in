/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 21:04:09 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/02 17:05:30 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	unsigned char	*p;

	p = (unsigned char *)malloc(sizeof(unsigned char) * size);
	if (!p)
		return (NULL);
	ft_bzero(p, size);
	return ((void *)p);
}
