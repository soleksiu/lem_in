/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 17:45:05 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/07 18:14:26 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstiter(t_list *lst, void (*f)(t_list *elem))
{
	t_list	*ptelem;

	if (lst && f)
	{
		ptelem = lst;
		while (ptelem->next)
		{
			f(ptelem);
			ptelem = ptelem->next;
		}
		f(ptelem);
	}
}
