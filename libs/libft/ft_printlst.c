/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printlst.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 14:08:35 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/13 14:03:57 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

static void	ft_printlast(t_list *p, size_t i)
{
	write(1, "List el#", 8);
	ft_putnbr(i);
	write(1, "\n", 1);
	write(1, "      Content: ", 15);
	ft_putendl((char*)(p->content));
	write(1, "Size in bytes: ", 15);
	ft_putnbr(p->content_size);
	write(1, "\n", 1);
}

void		ft_printlst(t_list *begin_list)
{
	t_list	*p;
	size_t	i;

	i = 1;
	if (begin_list)
	{
		p = begin_list;
		while (p->next)
		{
			write(1, "List el#", 8);
			ft_putnbr(i);
			write(1, "\n", 1);
			write(1, "      Content: ", 15);
			ft_putendl((char*)(p->content));
			write(1, "Size in bytes: ", 15);
			ft_putnbr(p->content_size);
			write(1, "\n\n", 2);
			p = p->next;
			i++;
		}
		ft_printlast(p, i);
	}
}
