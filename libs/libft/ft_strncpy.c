/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 17:26:18 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/09 17:40:55 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t	i;
	char	*p;

	i = 0;
	if (ft_strlen(src) <= len)
	{
		p = ft_strnew(len);
		ft_strcpy(p, (char *)src);
		src = p;
	}
	while (i <= len - 1 && len)
	{
		dst[i] = src[i];
		i++;
	}
	return (dst);
}
