/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clean_after_split.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 21:56:49 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/04 21:56:50 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_clean_after_split(char **str)
{
	unsigned int	i;

	i = 0;
	if (!str)
		return ;
	while (str[i])
		ft_strdel(&str[i++]);
	free(str);
	str = NULL;
}
