/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbits.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 12:41:21 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/13 12:53:03 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_printbits(unsigned char octet)
{
	unsigned char div;

	div = 128;
	while (div)
	{
		if (div <= octet)
		{
			write(1, "1", 1);
			octet = octet % div;
		}
		else
			write(1, "0", 1);
		div = div / 2;
	}
}
