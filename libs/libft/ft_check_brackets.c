/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_brackets.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/27 16:13:28 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/27 16:13:30 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static char	*ft_create_brackets_array(char *str, int *pairs_nb)
{
	char			*brackets_array;
	int				i;
	unsigned int	ar_size;

	i = 0;
	ar_size = 0;
	while (str[i])
	{
		if (str[i] == '[' || str[i] == ']' || str[i] == '('
			|| str[i] == ')' || str[i] == '{' || str[i] == '}')
			ar_size++;
		i++;
	}
	brackets_array = (char *)ft_memalloc(ar_size + 1);
	*pairs_nb = ar_size / 2;
	while (--i >= 0)
	{
		if (str[i] == '[' || str[i] == ']' || str[i] == '('
			|| str[i] == ')' || str[i] == '{' || str[i] == '}')
			brackets_array[--ar_size] = str[i];
	}
	return (brackets_array);
}

static void	ft_iterate_brackets(char *br_arr, int max_pairs)
{
	int		j;
	int		k;

	while (max_pairs--)
	{
		j = 0;
		while (br_arr[j])
		{
			if (br_arr[j] == '{' || br_arr[j] == '(' || br_arr[j] == '[')
			{
				k = j;
				j++;
				while (br_arr[j] == '1')
					j++;
				if ((br_arr[j] == (br_arr[k] + 1)) ||
					(br_arr[j] == (br_arr[k] + 2)))
				{
					br_arr[j] = '1';
					br_arr[k] = '1';
				}
				j = k;
			}
			j++;
		}
	}
}

int			ft_check_brackets(char *str)
{
	char	*br_arr;
	int		max_pairs;
	int		i;

	i = 0;
	br_arr = ft_create_brackets_array(str, &max_pairs);
	if (!br_arr[i])
		return (1);
	ft_iterate_brackets(br_arr, max_pairs);
	i = 0;
	while (br_arr[i])
	{
		if (br_arr[i] != '1')
			return (0);
		i++;
	}
	if (br_arr)
		free(br_arr);
	return (1);
}
