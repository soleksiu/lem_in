/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 17:10:55 by soleksiu          #+#    #+#             */
/*   Updated: 2017/12/30 16:41:36 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

static int			ft_checkfd(t_gnl **listptr, int fd)
{
	t_gnl	*tmp;

	tmp = *listptr;
	while (tmp)
	{
		if (tmp->fd == fd)
		{
			*listptr = tmp;
			return (1);
		}
		tmp = tmp->next;
	}
	return (0);
}

static int			ft_addlstel(t_gnl **filesinfo, int fd)
{
	t_gnl	*new;

	if (!(new = (t_gnl *)malloc(sizeof(t_gnl))))
		return (-1);
	new->fd = fd;
	new->aftersln = NULL;
	new->buf = NULL;
	new->next = NULL;
	new->sln = NULL;
	new->tofree = new->aftersln;
	if (*filesinfo)
	{
		new->next = *filesinfo;
		*filesinfo = new;
		return (1);
	}
	*filesinfo = new;
	return (1);
}

static char			*ft_expandstr(char *resstr, char *buf, t_gnl **listptr)
{
	char	*newstr;

	if (!buf || !(newstr = (char *)malloc(ft_strlen(resstr) + BUFF_SIZE + 1)))
		return (NULL);
	ft_strcpy(newstr, resstr);
	ft_strcat(newstr, buf);
	ft_strdel(&resstr);
	ft_memset((void*)(*listptr)->buf, 0, BUFF_SIZE + 1);
	return (newstr);
}

static void			ft_linecrt(t_gnl **listptr, char **line, int *r)
{
	if ((*listptr)->aftersln)
	{
		if (((*listptr)->sln = ft_strchr((*listptr)->aftersln, '\n')) != NULL)
		{
			*((*listptr)->sln) = '\0';
			*line = ft_expandstr(*line, (*listptr)->aftersln, listptr);
			(*listptr)->tofree = (*listptr)->aftersln;
			(*listptr)->aftersln = ft_strdup((*listptr)->sln + 1);
			ft_memdel((void*)&((*listptr)->tofree));
			return ;
		}
		*line = ft_expandstr(*line, (*listptr)->aftersln, listptr);
		ft_memdel((void*)&((*listptr)->aftersln));
	}
	while ((*r = read((*listptr)->fd, (*listptr)->buf, BUFF_SIZE)) > 0)
	{
		if (((*listptr)->sln = ft_strchr((*listptr)->buf, '\n')) != NULL)
		{
			*((*listptr)->sln) = '\0';
			(*listptr)->aftersln = ft_strdup((*listptr)->sln + 1);
			*line = ft_expandstr(*line, (*listptr)->buf, listptr);
			break ;
		}
		*line = ft_expandstr(*line, (*listptr)->buf, listptr);
	}
}

int					ft_get_next_line(const int fd, char **line)
{
	static t_gnl	*filesinfo;
	t_gnl			*listptr;
	int				r;

	r = 1;
	if (!line || !(*line = ft_strnew(BUFF_SIZE + 1)))
		return (-1);
	if (fd == -1 || BUFF_SIZE < 1 || fd > 4864 || read(fd, *line, 0) == -1)
	{
		ft_memdel((void*)line);
		return (-1);
	}
	if (!filesinfo && (ft_addlstel(&filesinfo, fd) == -1))
		return (-1);
	listptr = filesinfo;
	if (!(ft_checkfd(&listptr, fd)) && (ft_addlstel(&filesinfo, fd)))
		listptr = filesinfo;
	listptr->buf = ft_strnew(BUFF_SIZE + 1);
	ft_linecrt(&listptr, line, &r);
	ft_memdel((void*)&(listptr->buf));
	if (**line && !r)
		return (1);
	if (!**line && r == 0 && filesinfo)
		ft_memdel((void*)line);
	return (r >= 1 ? 1 : 0);
}
