/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:13:05 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/08 18:30:01 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strtrim(char const *s)
{
	char	*b;
	char	*e;
	char	*n;

	b = (char *)s;
	e = (char *)s;
	if (!s)
		return (NULL);
	while (*b == ' ' || (*b >= '\b' && *b <= '\r'))
		b++;
	if (*b == '\0')
	{
		if (!(n = ft_strnew(1)))
			return (NULL);
		return (n);
	}
	while (*(e + 1))
		e++;
	while (*e == ' ' || (*e >= '\b' && *e <= '\r'))
		e--;
	if (!(n = ft_strnew((e - b) + 1)))
		return (NULL);
	ft_strncpy(n, b, (e - b) + 1);
	return (n);
}
