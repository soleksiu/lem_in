/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 11:42:48 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/13 12:13:21 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_max(int *ar, size_t arlen)
{
	int		max;
	size_t	i;

	i = 0;
	max = 0;
	if (*ar && arlen)
	{
		max = ar[0];
		while (i < arlen)
		{
			if (ar[i] > max)
				max = ar[i];
			i++;
		}
	}
	return (max);
}
