/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_ways.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 18:21:36 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/12 18:21:38 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	ft_check_for_short(t_lemin *data)
{
	t_list	*start_neib;

	start_neib = data->start->adj_lst;
	while (start_neib)
	{
		if (start_neib->content == data->end)
			return (1);
		start_neib = start_neib->next;
	}
	return (0);
}

/*
** Finding all possible ways will be made here:
** 		0. Checking for short way - case when 'start' and 'end' connected;
**		In each step :
** 		1. Finding way in sft_find_path_bfs
**		  where vertices which belongs one way will be marked;
**		2. Based on that information way will be reproduced in ft_get_way;
**		3. Reseting params of vertices to defaults in ft_reset_vertices_params;
*/

void		ft_find_ways(t_lemin *data)
{
	if (!data->short_way && ft_check_for_short(data))
	{
		data->start->from = data->end;
		data->short_way = 1;
		data->ways_found = 1;
		data->ways_available--;
		ft_get_way(data);
		data->end->from = NULL;
		ft_reset_vertices_params(data);
		return ;
	}
	while (data->ways_available)
	{
		ft_find_path_bfs(data);
		if (data->all_ways_found)
			break ;
		ft_get_way(data);
		ft_reset_vertices_params(data);
		data->ways_available--;
		data->end->from = NULL;
	}
}
