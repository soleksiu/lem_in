/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output_lems.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 19:06:29 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/21 20:48:04 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_output_short_way(t_lemin *data)
{
	int	lem;

	lem = 0;
	while (++lem <= data->lem_nb)
		ft_printf("L%u-%s%c", lem, data->end->name,
			(lem == data->lem_nb ? '\n' : ' '));
}

/*
** Ants management and position output:
** 		0. If end and start connected with the link, all ants will be moved to
**		  'end' in one step. Output will fit in one line(ft_output_short_way);
**		In each step :
** 		1. Ants will be distributed to ways using ft_distribute_ants_to_ways;
**		2. Ants in ways will be moved for one position (ft_move_ants);
**		3. Current ants position will be displayed using ft_output_ants_pos;
** 		4. Loop will end when all ants arrive to the 'end' room;
*/

void		ft_ants_move_and_output(t_lemin *data)
{
	int		lem;
	int		way_ind;

	lem = 1;
	if (data->short_way)
	{
		ft_output_short_way(data);
		return ;
	}
	while (data->end->lem_inside < data->lem_nb)
	{
		if (lem != -1)
			ft_distribute_ants_to_ways(data, &lem);
		way_ind = -1;
		while (data->ways[++way_ind])
		{
			if (data->ways[way_ind]->in_use)
			{
				ft_move_ants(data, way_ind);
				data->ways[way_ind]->ant_to_add = 0;
			}
		}
		ft_output_ants_pos(data, data->ways);
	}
}
