/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_data.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 20:46:18 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 18:33:17 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_check_lems(t_lemin *data, const char *str, int size)
{
	if (size != 1 || !ft_isnumeric(str) || str[0] == '-')
		ft_error(data, "wrong ants number");
	data->lem_nb = ft_atoi(str);
	if (data->lem_nb <= 0)
		ft_error(data, "wrong ants number");
	data->lem_nb_ind = data->cur_ln_ind;
}

static void	ft_check_comments(t_lemin *data, size_t size)
{
	data->lst_com_ind = data->cur_ln_ind;
	if (data->flag_wall)
	{
		if (size > 1)
			ft_error(data, "wrong input! - multi word comments is not allowed");
		if (!data->lem_nb)
			ft_error(data, "wrong input! - comment before ants");
		if (data->cur_ln_ind - 1 == data->lem_nb_ind)
			ft_error(data, "wrong input! - comments after ants");
		if (data->cur_ln_ind - 1 == data->start_cm_ind)
			ft_error(data, "wrong input! - comments after start command");
		if (data->cur_ln_ind - 1 == data->end_cm_ind)
			ft_error(data, "wrong input! - comments after end command");
	}
}

void		ft_parse_data(t_lemin *d, size_t *i)
{
	const char		**splt_ln;
	size_t			splt_size;

	splt_ln = (const char **)ft_strsplit(d->line, ' ');
	splt_size = ft_strings_array_size(splt_ln);
	if (splt_ln[0] && splt_ln[0][0] == '#' && splt_ln[0][1] != '#')
		ft_check_comments(d, splt_size);
	else if (splt_ln[0] && splt_ln[0][0] == '#' &&
			splt_ln[0][1] == '#')
		ft_parse_command(d, *i);
	else if (!d->inp_e && !d->lem_nb && splt_size == 1)
		ft_check_lems(d, splt_ln[0], splt_size);
	else if (!d->inp_e && splt_size == 3)
	{
		ft_lstpushback(&(d->graph_lst),
			ft_lstnew(ft_parse_vertex(d, splt_ln), 0));
		(*i)++;
	}
	else if (!d->inp_e && splt_size == 1 && ft_strchr(splt_ln[0], '-'))
		ft_parse_edge(d, d->line);
	else
		d->inp_e = 1;
	ft_clean_after_split((char **)splt_ln);
}
