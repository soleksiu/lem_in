/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_path_bfs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 18:23:33 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/12 18:23:34 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_check_and_free_quene(t_lemin *data, t_vertex **queue, size_t ind)
{
	if (queue[ind] == data->start)
		data->ways_found++;
	else if (queue[ind] == NULL && data->ways_found != 0)
		data->all_ways_found = 1;
	else if (queue[ind] == NULL && data->ways_found == 0)
		ft_error(data, "there is no possible ways found!");
	free(queue);
}

static void	ft_queuepushback(t_vertex **queue, t_vertex *elem)
{
	size_t	i;

	i = 0;
	while (queue[i])
	{
		if (elem == queue[i])
			return ;
		i++;
	}
	queue[i] = elem;
}

static void	ft_create_queue(t_lemin *data)
{
	data->queue = (t_vertex **)ft_memalloc(data->vertex_nb *
		data->vertex_nb * sizeof(t_vertex *));
	data->queue[0] = data->end;
	data->end->visited = 1;
}

/*
** Finding shortest way in graph using Breadth-First Search algorithm:
** 		0. Creating the queue, which storing vertices addresses;
**		In each step :
** 		1. Getting all connected neighbours for current vertice;
**		2. Adding them to the end of quene using ft_queuepushback;
**		3. Each added vertice storing the address of vertice from which we came;
** 		4. Loop will end when:
** 			4.1 Quene become empty - in this case there is no possible ways.
** 			4.1 The address of 'start' room will come to the begin of queue.
**			    In this case way found sucessfully. Note that this algo is
**			    working from end to start this made to optimising
** 			    the ways reproduction in ft_get_way;
** 		5. Clearing the queue and if no ways found the error will be displayed;
*/

void		ft_find_path_bfs(t_lemin *data)
{
	size_t		i;
	t_list		*neib;
	t_vertex	*cur_vertex;

	i = 0;
	ft_create_queue(data);
	while ((data->queue[i]) != NULL && (data->queue[i]) != data->start)
	{
		neib = (data->queue[i])->adj_lst;
		while (neib)
		{
			cur_vertex = (t_vertex *)neib->content;
			if (cur_vertex == data->start && data->queue[i] == data->end)
				continue ;
			else if (cur_vertex->visited == 0)
			{
				cur_vertex->visited = 1;
				cur_vertex->from = data->queue[i];
				ft_queuepushback(&data->queue[i], neib->content);
			}
			neib = neib->next;
		}
		i++;
	}
	ft_check_and_free_quene(data, data->queue, i);
}
