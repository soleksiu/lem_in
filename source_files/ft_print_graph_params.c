/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_graph_params.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 22:42:18 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/07 22:42:19 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_print_connected_vert(t_vertex *vertex)
{
	t_list *tmp;

	tmp = vertex->adj_lst;
	ft_printf("Connected with vertices: ");
	if (tmp == NULL)
	{
		ft_printf("none\n");
		return ;
	}
	while (tmp->next)
	{
		ft_printf("{yellow}%s {eoc}", ((t_vertex *)(tmp->content))->name);
		tmp = tmp->next;
	}
	ft_printf("{yellow}%s{eoc}", ((t_vertex *)(tmp->content))->name);
	ft_printf("\n");
}

static void	ft_print_edges(t_vertex *vertex)
{
	t_list *tmp;

	tmp = vertex->adj_lst;
	ft_printf("Edges list:\n");
	if (tmp == NULL)
	{
		ft_printf("none\n");
		return ;
	}
	while (tmp->next)
	{
		ft_printf("%s-", vertex->name);
		ft_printf("%s\n", ((t_vertex *)(tmp->content))->name);
		tmp = tmp->next;
	}
	ft_printf("%s-", vertex->name);
	ft_printf("%s\n", ((t_vertex *)(tmp->content))->name);
}

/*
** Function to display alternative info about map.
** Will be called if flag -m is used.
*/

void		ft_print_graph_params(t_lemin *data)
{
	size_t i;

	i = 0;
	ft_printf("{bold}Graph params:{eoc}\n");
	ft_printf(" *ants nb: %u\n *vert nb: %u\n", data->lem_nb, data->vertex_nb);
	ft_printf(" *edge nb: %u\n *start: ", data->edge_nb);
	ft_printf("\n	vertex name: %s | ", data->start->name);
	ft_printf("x = %g y = %g\n", data->start->coords.x, data->start->coords.y);
	ft_printf(" *end:\n	vertex name: %s | ", data->end->name);
	ft_printf("x = %g y = %g\n", data->end->coords.x, data->end->coords.y);
	ft_printf("__________________________________________________\n");
	ft_printf("{underline}Vertices info:{eoc}\n");
	while (data->graph[i])
	{
		ft_printf("Vertex name: {green}%s{eoc} | crds: ", data->graph[i]->name);
		ft_printf("x = %.f ", data->graph[i]->coords.x);
		ft_printf("y = %.f\n", data->graph[i]->coords.y);
		ft_print_connected_vert(data->graph[i]);
		ft_print_edges(data->graph[i]);
		i++;
		if (data->graph[i])
			ft_printf("***************************************************\n");
	}
	ft_printf("___________________________________________________\n\n");
}
