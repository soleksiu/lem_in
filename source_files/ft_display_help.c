/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_help.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 15:09:50 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/15 15:09:51 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

#define DESCRIPTION "This project is meant to create "
#define DESCRIPTION2 "an ant farm manager."
#define FL_M "   -m, --map_info		display detailed map info\n"
#define FL_P "   -p, --paths 			display available paths\n"
#define FL_L "   -wl, --ways_limit 		set max number of ways(val.: 1 - all)\n"
#define FL_WA "   -Wall, --warn_all		use rigourus checks for errors\n"
#define HELP "   -h, --help 			display this help and exit\n"
#define INFO "   -i, --info 			display info about project\n"
#define FL_E "   -e, --errors_extended	enable additional info about errors\n"
#define FL_Q "   -q, --quiet			suppress all output except ants move\n"
#define FL_S "   -s, --sort			sort output depending to ant number\n"
#define S "______________________________________________"

/*
** Function for displaying help information.
*/

void	ft_display_help(t_lemin *data)
{
	ft_printf("{bold} {cyan}\r%s%s\n{eoc}", DESCRIPTION, DESCRIPTION2);
	ft_printf("%s%s\n", S, S);
	ft_error(data, "usage");
	ft_printf("%s%s\n", S, S);
	ft_printf("Available flags description:\n");
	write(1, FL_E, ft_strlen(FL_E));
	write(1, HELP, ft_strlen(HELP));
	write(1, INFO, ft_strlen(INFO));
	write(1, FL_M, ft_strlen(FL_M));
	write(1, FL_P, ft_strlen(FL_P));
	write(1, FL_Q, ft_strlen(FL_Q));
	write(1, FL_S, ft_strlen(FL_S));
	write(1, FL_L, ft_strlen(FL_L));
	write(1, FL_WA, ft_strlen(FL_WA));
	ft_printf("%s%s\n", S, S);
	exit(0);
}
