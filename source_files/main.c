/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:10:09 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 19:36:22 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

static void	ft_set_start_params(t_lemin *data)
{
	if (data->flag_wl && data->flag_wl < 42)
		data->ways_available = data->flag_wl;
	if (!data->ways_available)
		data->ways_available = 42;
	data->start_ind = -1;
	data->end_ind = -1;
}

/*
** to test with lem-in_test (https://bitbucket.org/Tbouder/lem-in_test)
** add after line 40 this code:
**		lemin_data.flag_wall = 1;
**      lemin_data.flag_s = 1;
** or use flags -Wall -s and run maps from it manually
*/

int			main(int argc, char **argv)
{
	t_lemin	lemin_data;

	ft_memset((void *)&lemin_data, 0, sizeof(lemin_data));
	if (argc > 1)
		ft_flags_manager(&lemin_data, argc, argv);
	ft_set_start_params(&lemin_data);
	ft_read_input(&lemin_data);
	ft_find_ways(&lemin_data);
	if (!lemin_data.flag_q && lemin_data.flag_m)
		ft_print_graph_params(&lemin_data);
	else if (!lemin_data.flag_q)
		ft_output_map(&lemin_data);
	if (lemin_data.flag_p)
		ft_print_ways(&lemin_data);
	ft_lstdel(&lemin_data.lst, ft_del_link);
	ft_ants_move_and_output(&lemin_data);
	return (0);
}
