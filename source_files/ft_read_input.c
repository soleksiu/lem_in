/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 18:40:28 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 15:22:37 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_make_flag_wall_checks(t_lemin *data)
{
	if (data->cur_ln_ind == data->lst_com_ind)
		ft_error(data, "wrong input! - comments in the end of input");
	if (data->start_cm_ind - 1 == data->lst_com_ind)
		ft_error(data, "wrong input! - commands before start is not allowed!");
	if (data->end_cm_ind - 1 == data->lst_com_ind)
		ft_error(data, "wrong input! - commands before end is not allowed!");
}

static void	ft_check_data_after_parsing(t_lemin *data)
{
	if (!data->lem_nb)
		ft_error(data, "wrong input - no number of ants");
	if (data->start_ind == -1 || data->start_ind >= (int)data->vertex_nb ||
		data->end_ind == -1 || data->end_ind >= (int)data->vertex_nb
	|| data->end_ind == data->start_ind)
		ft_error(data, "wrong input - no start or end specified");
	if (data->flag_wall)
		ft_make_flag_wall_checks(data);
	if (!data->rooms_readed)
		ft_error(data, "wrong input - no links");
	data->start = data->graph[(size_t)data->start_ind];
	data->end = data->graph[(size_t)data->end_ind];
	ft_check_graph_for_errors(data);
}

/*
** Getting data from stdin and putting it to list.
** Each line will be parsed and checked for errors.
** Last stage of validation will be made in ft_check_data_after_parsing.
*/

void		ft_read_input(t_lemin *data)
{
	size_t	i;

	i = 0;
	data->input_processing = 1;
	while (ft_get_next_line(0, &data->line) > 0)
	{
		data->cur_ln_ind++;
		ft_parse_data(data, &i);
		if (data->inp_e)
		{
			ft_memdel((void *)&data->line);
			break ;
		}
		ft_lstpushback(&data->lst, ft_lstnew(data->line,
			ft_strlen(data->line) + 1));
		ft_memdel((void *)&data->line);
	}
	data->input_processing = 0;
	ft_check_data_after_parsing(data);
}
