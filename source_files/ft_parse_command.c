/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_command.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 20:57:38 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 15:41:12 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_parse_st_end(t_lemin *data, int i)
{
	if (ft_strnstr(data->line, "##start", 8))
	{
		if (data->start_ind != -1)
			ft_error(data, "wrong input! - incorrect command format");
		data->start_ind = i;
		data->start_cm_ind = data->cur_ln_ind;
	}
	else if (ft_strnstr(data->line, "##end", 6))
	{
		if (data->end_ind != -1)
			ft_error(data, "wrong input! - incorrect command format");
		data->end_ind = i;
		data->end_cm_ind = data->cur_ln_ind;
	}
}

/*
** Commands will be parsed and checked for errors here.
*/

void		ft_parse_command(t_lemin *data, int i)
{
	if (data->line[0] != '#')
	{
		data->inp_e = 1;
		return ;
	}
	if (data->flag_wall && ft_strlen(data->line) > 2 && data->line[2] == '#')
		ft_error(data, "wrong input! - incorrect command format");
	if (ft_strnstr("##start", data->line, 8) ||
		ft_strnstr("##end", data->line, 6))
		ft_parse_st_end(data, i);
	if (data->flag_wall)
	{
		if (data->cur_ln_ind - 1 == data->start_cm_ind ||
			data->cur_ln_ind - 1 == data->end_cm_ind)
			ft_error(data, "wrong input! - wrong line after start/end command");
		if (data->cur_ln_ind - 1 == data->lem_nb_ind)
			ft_error(data, "wrong input! - commands after ants is not allowed");
		if (!data->start_cm_ind)
			ft_error(data, "wrong input! - commands before start not allowed");
	}
}
