/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_graph_for_errors.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 18:12:57 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/16 21:57:38 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_check_edges_nb(t_lemin *data)
{
	t_vertex	**graph_pt;
	size_t		i;
	size_t		degree_sum;

	i = 0;
	degree_sum = 0;
	graph_pt = data->graph;
	while (graph_pt[i])
		degree_sum += graph_pt[i++]->degree;
	if (degree_sum % 2 != 0)
		ft_error(data, "wrong graph!");
}

/*
** Additional checks for errors after map parsing.
*/

void		ft_check_graph_for_errors(t_lemin *data)
{
	if (!data->lem_nb)
		ft_error(data, "wrong input - no lems number");
	if (!data->vertex_nb)
		ft_error(data, "wrong input - no verteces");
	if (!data->edge_nb)
		ft_error(data, "wrong input - no edges");
	if (data->flag_wall)
		ft_check_edges_nb(data);
	if (data->start->degree == 0)
		ft_error(data, "wrong input - start is not connected to graph");
	if (data->end->degree == 0)
		ft_error(data, "wrong input - end is not connected to graph");
}
