/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_way.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 18:23:04 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/21 13:41:23 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static size_t	ft_get_way_len(t_lemin *data)
{
	size_t		len;
	t_vertex	*pt;

	len = 1;
	pt = data->start;
	while (pt)
	{
		if (pt != data->start && pt != data->end)
			len++;
		pt = pt->from;
	}
	return (len);
}

static t_vertex	*ft_create_end_copy(t_lemin *data)
{
	t_vertex	*ret;

	ret = NULL;
	if (!(ret = (t_vertex*)ft_memalloc(sizeof(t_vertex))))
		ft_error(data, "allocation for end copy fails!");
	ft_memcpy(ret, data->end, sizeof(t_vertex));
	return (ret);
}

static void		ft_calculate_delta(t_lemin *data, int way_ind)
{
	int		delta;
	int		i;
	size_t	cur_len;
	size_t	shorter_way_len;

	i = way_ind;
	delta = 0;
	cur_len = data->ways[way_ind]->len;
	while (i >= 0)
	{
		shorter_way_len = data->ways[i]->len;
		delta += cur_len - shorter_way_len;
		i--;
	}
	data->ways[way_ind]->delta = delta;
}

/*
** Function to allocate the memory for way, fill the way
** with vertices addresses, counting the way length.
** Also parameter called delta will be calculated here.
** It will be used to distribute ants.
*/

void			ft_get_way(t_lemin *data)
{
	t_vertex	*pt;
	size_t		way_len;
	t_vertex	**way_pt;
	size_t		i;

	i = 0;
	way_len = ft_get_way_len(data);
	pt = data->start;
	data->ways[data->ways_found - 1]->way =
	(t_vertex **)ft_memalloc((way_len + 2) * sizeof(t_vertex *));
	data->ways[data->ways_found - 1]->len = way_len;
	way_pt = data->ways[data->ways_found - 1]->way;
	while (pt)
	{
		if (pt == data->end)
			way_pt[i++] = ft_create_end_copy(data);
		else
			way_pt[i++] = pt;
		if (pt != data->start && pt != data->end)
			pt->in_way = data->ways_found;
		pt = pt->from;
	}
	if (!data->min_way_len)
		data->min_way_len = way_len;
	ft_calculate_delta(data, data->ways_found - 1);
}
