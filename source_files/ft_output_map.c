/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 22:08:32 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/21 22:08:34 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** Function to print map in final output.
*/

void		ft_output_map(t_lemin *data)
{
	t_list	*map;

	map = data->lst;
	while (map)
	{
		if (map->content_size != INT_MAX)
		{
			ft_putstr(map->content);
			ft_putchar('\n');
		}
		map = map->next;
	}
	ft_putchar('\n');
}
