/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_ways.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 13:22:00 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/13 23:54:54 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** Function to display info about finded ways.
** Will be called if flag -p is used.
*/

void	ft_print_ways(t_lemin *data)
{
	size_t		i;
	size_t		j;
	t_vertex	**cur_way;

	i = -1;
	ft_printf("{bold}Here is an info about available paths:{eoc}\n");
	ft_printf("Paths found: {cyan}%d\n{eoc}", data->ways_found);
	ft_printf("Start room:  {cyan}%s\n{eoc}", data->start->name);
	ft_printf("End room:    {cyan}%s\n{eoc}", data->end->name);
	ft_printf("_________________________________________________\n");
	while (++i < data->ways_found)
	{
		cur_way = data->ways[i]->way;
		ft_printf("The way {green}#%u{eoc} with lenght {yellow}%u{eoc} is: \n",
		i + 1, data->ways[i]->len);
		j = -1;
		while (cur_way[++j])
		{
			ft_printf("%s", cur_way[j]->name);
			ft_printf("%s", cur_way[j + 1] ? "-->" : "\n");
		}
		if (i + 1 < data->ways_found)
			ft_printf("************************************************\n");
	}
	ft_printf("_________________________________________________\n\n");
}
