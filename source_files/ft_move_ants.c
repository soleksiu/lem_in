/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_ants.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 22:39:16 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/21 22:39:17 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static size_t	ft_find_empty_room(t_lemin *data, t_vertex **way_pt)
{
	size_t	i;

	i = 0;
	(void)data;
	while (way_pt[i + 1])
	{
		if (way_pt[i] != data->start && way_pt[i]->lem_inside == 0)
			return (i);
		i++;
	}
	return (i);
}

/*
** Function to move ants in way.
** It will find the ants which in the 'head' of column and
** move all ants for one position in way.
** If in function ft_distribute_ants new ant was added to this way,
** it will be placed to the first position in way.
*/

void			ft_move_ants(t_lemin *data, size_t way_i)
{
	t_vertex	*cur_vertex;
	t_vertex	*prev_vertex;
	size_t		fst_ant_pos;

	if (data->end->lem_inside == data->lem_nb || !data->ways[way_i]->in_use)
		return ;
	if (!data->ways[way_i]->ind_of_head)
	{
		fst_ant_pos = data->ways[way_i]->ant_to_add == -1 ? data->ways[0]->len :
		ft_find_empty_room(data, data->ways[way_i]->way);
		data->ways[way_i]->ind_of_head = fst_ant_pos;
	}
	else
		fst_ant_pos = data->ways[way_i]->ind_of_head;
	while (fst_ant_pos - 1 != 0)
	{
		cur_vertex = data->ways[way_i]->way[fst_ant_pos];
		prev_vertex = data->ways[way_i]->way[fst_ant_pos-- - 1];
		cur_vertex->lem_inside = prev_vertex->lem_inside;
		prev_vertex->lem_inside = 0;
	}
	if (data->ways[way_i]->ant_to_add != -1)
		data->ways[way_i]->way[1]->lem_inside = data->ways[way_i]->ant_to_add;
	if (data->ways[way_i]->ind_of_head < data->ways[way_i]->len)
		data->ways[way_i]->ind_of_head++;
}
