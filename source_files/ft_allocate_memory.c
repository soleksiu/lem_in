/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_allocate_memory.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 22:25:08 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 19:00:38 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_copy_from_list(t_lemin *data)
{
	size_t	i;
	t_list	*lst_pt;

	i = 0;
	lst_pt = data->graph_lst;
	while (i < data->vertex_nb)
	{
		data->graph[i] = (t_vertex *)lst_pt->content;
		i++;
		lst_pt = lst_pt->next;
	}
}

/*
** Memory for graph and ways will be allocated here:
** 		1. Amount of memory needed for allocation based on vertices number,
**		which was calculated in ft_read_input;
**		2. Graph will be defined as an array of the addresses of vertices;
**		3. Ways will storing as array of the addresses of structures s_way;
** 		4. Each way will be defined as array of addresses of vertices;
*/

void		ft_allocate_memory(t_lemin *data)
{
	int	i;

	i = 0;
	if (!(data->graph = (t_vertex**)ft_memalloc(sizeof(t_vertex *) *
		(data->vertex_nb + 1))))
		ft_error(data, "allocation for graph fails!");
	data->graph[data->vertex_nb] = NULL;
	ft_copy_from_list(data);
	if (!(data->ways = (t_way **)ft_memalloc(sizeof(t_way *) *
		(data->ways_available + 1))))
		ft_error(data, "allocation for ways array fails!");
	while (i < data->ways_available)
		data->ways[i++] = ft_memalloc(sizeof(t_way));
	data->ways[data->ways_available] = NULL;
}
