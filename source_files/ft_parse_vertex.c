/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_vertex.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 18:56:01 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 20:55:10 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_check_duplicates(t_lemin *data, const char **arr)
{
	t_list		*lst_pt;
	t_vertex	*cur_vert;

	lst_pt = data->graph_lst;
	if (!lst_pt)
		return ;
	while (lst_pt)
	{
		cur_vert = (t_vertex *)lst_pt->content;
		if (cur_vert->name && ft_strequ(cur_vert->name, arr[0]))
			ft_error(data, "wrong room  - couple rooms with same name");
		if (cur_vert->coords.x == ft_atoi(arr[1]) &&
			cur_vert->coords.y == ft_atoi(arr[2]))
			ft_error(data, "wrong room - couple rooms with same coords");
		lst_pt = lst_pt->next;
	}
}

static void	ft_check_for_errors(t_lemin *data, const char **arr)
{
	size_t	i;
	int		spaces_nb;

	i = 0;
	spaces_nb = 0;
	if (ft_strings_array_size(arr) != 3)
		ft_error(data, "wrong line!");
	if (arr[0][0] == 'L' || arr[0][0] == '#' || ft_strchr(arr[0], '-'))
		ft_error(data, "wrong room name");
	if (!ft_isnumeric(arr[1]) || !ft_isnumeric(arr[2]))
		ft_error(data, "wrong room coords - only integers accepted");
	if (data->flag_wall)
	{
		while (data->line[i])
			if (data->line[i++] == ' ')
				spaces_nb++;
		if (spaces_nb > 2)
			ft_error(data, "wrong room description");
	}
	if (data->rooms_readed)
		ft_error(data, "wrong input - rooms in links section or "
				"links in rooms section");
}

/*
** Vertices infor will be parsed and checked for errors here.
*/

t_vertex	*ft_parse_vertex(t_lemin *data, const char **arr)
{
	t_vertex	*vertex;
	int			coord_x;
	int			coord_y;

	vertex = NULL;
	ft_check_for_errors(data, arr);
	coord_x = ft_atoi(arr[1]);
	coord_y = ft_atoi(arr[2]);
	ft_check_duplicates(data, arr);
	vertex = (t_vertex *)ft_memalloc(sizeof(t_vertex));
	vertex->name = ft_strdup(arr[0]);
	vertex->coords.x = coord_x;
	vertex->coords.y = coord_y;
	vertex->degree = 0;
	vertex->adj_lst = NULL;
	data->vertex_nb++;
	return (vertex);
}
