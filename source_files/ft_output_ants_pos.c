/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output_ants_pos.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 22:36:49 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/21 22:36:50 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	ft_check_if_in_use(t_vertex **way_pt)
{
	size_t	i;

	i = 0;
	while (way_pt[i + 1])
	{
		if (way_pt[i]->lem_inside != 0)
			return (1);
		i++;
	}
	return (0);
}

static void	ft_add_info_to_list(t_list **alst, t_vertex *v)
{
	char *ant_nb;
	char *left_side;
	char *right_side;
	char *res;

	ant_nb = ft_itoa(v->lem_inside);
	left_side = ft_strjoin("L", ant_nb);
	right_side = ft_strjoin("-", v->name);
	res = ft_strjoin(left_side, right_side);
	free(ant_nb);
	free(left_side);
	free(right_side);
	free(res);
	ft_lstpushback(alst, ft_lstnew(res, ft_strlen(res) + 1));
}

static void	ft_sort_output(t_list *lst)
{
	char	*cur_elem;
	char	*next_elem;
	int		list_len;
	void	*buf;
	t_list	*begin;

	list_len = ft_lstsize(lst);
	if (!lst->next)
		return ;
	while (list_len--)
	{
		begin = lst;
		while (begin->next)
		{
			cur_elem = (char *)begin->content;
			next_elem = (char *)begin->next->content;
			if (ft_atoi(cur_elem + 1) > ft_atoi(next_elem + 1))
			{
				buf = begin->content;
				begin->content = begin->next->content;
				begin->next->content = buf;
			}
			begin = begin->next;
		}
	}
}

static void	ft_output_line(t_lemin *data, t_list *lst)
{
	if (data->flag_s)
		ft_sort_output(data->lst);
	while (lst)
	{
		ft_printf("%s%c", lst->content, !lst->next ? '\n' : ' ');
		lst = lst->next;
	}
}

/*
** Getting current ants possitions in ways, storing them in list and printing.
** If -s flag will be used output will be sorted based on ant number.
*/

void		ft_output_ants_pos(t_lemin *data, t_way **ways)
{
	t_vertex	*v;
	size_t		ants_last;
	size_t		i;

	i = -1;
	while (ways[++i])
	{
		if (data->ways[i]->in_use == 0)
			continue ;
		ants_last = data->ways[i]->ind_of_head;
		while (ants_last != 0)
		{
			v = data->ways[i]->way[ants_last--];
			if (v->lem_inside)
				ft_add_info_to_list(&data->lst, v);
			if (v->lem_inside && ft_strequ(v->name, data->end->name))
				data->end->lem_inside++;
		}
		if (data->end->lem_inside == data->lem_nb)
			break ;
		if (!ft_check_if_in_use(data->ways[i]->way))
			data->ways[i]->in_use = 0;
	}
	ft_output_line(data, data->lst);
	ft_lstdel(&data->lst, ft_del_link);
}
