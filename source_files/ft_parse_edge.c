/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_edge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 19:01:06 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 23:09:26 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	ft_connect_vertex_to_list(t_lemin *data, int from, int to)
{
	t_list	*new_elem;

	if (!ft_lst_inlist(&(data->graph[from]->adj_lst), data->graph[to]))
	{
		new_elem = ft_lstnew(data->graph[to], 0);
		ft_lstpushback(&(data->graph[from]->adj_lst), new_elem);
		data->graph[from]->degree++;
	}
	if (!ft_lst_inlist(&(data->graph[to]->adj_lst), data->graph[from]))
	{
		new_elem = ft_lstnew(data->graph[from], 0);
		ft_lstpushback(&(data->graph[to]->adj_lst), new_elem);
		data->graph[to]->degree++;
	}
	data->edge_nb++;
	if (data->flag_wall && data->cur_ln_ind - 1 == data->lst_com_ind)
		ft_error(data, "wrong input! - commands after rooms is not allowed!");
}

static int	ft_check_multi_link(const char *str)
{
	size_t	links_nb;

	links_nb = 0;
	while (*str)
		if (*str++ == '-')
			links_nb++;
	return ((links_nb == 1 ? 0 : 1));
}

static int	ft_count_duplicates(t_lemin *data, const char *str)
{
	int		copy_nb;
	t_list	*lst;

	copy_nb = 0;
	lst = data->lst;
	while (lst)
	{
		if (ft_strequ(str, (const char*)lst->content))
			copy_nb++;
		lst = lst->next;
	}
	return (copy_nb);
}

static void	ft_chk_dupl(t_lemin *data, const char *str, const char **arr, int s)
{
	int		copy_nb;
	int		copy_nb_inverted;
	char	*to_check;
	int		sec_wrd_pos;

	if (!data->rooms_readed && (data->rooms_readed = 1))
		ft_allocate_memory(data);
	if (!data->flag_wall || s != 2)
		return ;
	to_check = NULL;
	sec_wrd_pos = ft_strlen(arr[1]);
	if (ft_strequ(arr[0], arr[1]))
		ft_error(data, "loop detected");
	to_check = ft_strnew(ft_strlen(str));
	ft_strcpy(to_check, arr[1]);
	to_check[sec_wrd_pos] = '-';
	ft_strcpy(to_check + sec_wrd_pos + 1, arr[0]);
	copy_nb = ft_count_duplicates(data, str);
	copy_nb_inverted = ft_count_duplicates(data, to_check);
	if (copy_nb > 0 || copy_nb_inverted == 1)
		ft_error(data, "edge duplicates detected");
	free(to_check);
}

/*
** Edges infor will be parsed and checked for errors here.
*/

void		ft_parse_edge(t_lemin *data, const char *str)
{
	int			i;
	int			from;
	int			to;
	const char	**splited_ln;
	size_t		splited_size;

	splited_ln = (const char **)ft_strsplit(str, '-');
	splited_size = ft_strings_array_size(splited_ln);
	ft_chk_dupl(data, str, splited_ln, splited_size);
	i = -1;
	from = -1;
	to = -1;
	while (data->graph[++i] && splited_size == 2)
	{
		if (from == -1 && ft_strequ(splited_ln[0], data->graph[i]->name))
			from = i;
		if (to == -1 && ft_strequ(splited_ln[1], data->graph[i]->name))
			to = i;
		if (from != -1 && to != -1)
			break ;
	}
	if (splited_size != 2 || from == -1 || to == -1 || ft_check_multi_link(str))
		ft_error(data, "wrong edge parameters!");
	ft_connect_vertex_to_list(data, from, to);
	ft_clean_after_split((char **)splited_ln);
}
