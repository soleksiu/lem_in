/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:05:50 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 21:11:04 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#define US "usage : lem-in [[-i | -h] | [-e -m -p -q -s -wl -Wall]] "
#define US2 "[input map parameters to stdin]"
#define US3 "        lem-in [[-i | -h] | [-e -m -p -q -s -wl -Wall]] "
#define US4 "< ant_farm_map [redirect data from file]"

/*
** Function for displaying error message or usage info.
*/

void		ft_error(t_lemin *data, char *str)
{
	if (ft_strequ(str, "usage"))
	{
		ft_printf("{bold}%s%s\n", US, US2);
		ft_printf("%s%s\n{eoc}", US3, US4);
		return ;
	}
	else
	{
		if (data->flag_e)
		{
			if (data->input_processing)
				ft_printf("{bold}line:%zu:", data->cur_ln_ind);
			ft_printf("{red} {bold}%s {white}%s{eoc}\n", "ERROR:", str);
		}
		else
			ft_printf("{red} {bold}\r%s{eoc}\n", "ERROR");
	}
	exit(-1);
}
