/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags_manager.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 15:10:10 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/15 15:10:11 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

#define DIMENSIONS_ERR "please enter correct dimensions. "
#define DIMENSIONS_ERR2 "use -h or --help flag for help."
#define ITER_ERR "please enter correct iterations number. "
#define ITER_ERR2 "use -h or --help flag for help."
#define COLOR_ERR "please enter correct color. use -h "
#define COLOR_ERR2 "or --help flag for help."
#define INFO_T "This is a learning project of 42 "
#define INFO_T2 "school(obviously the best place to code in the Galaxy).\n"
#define DESCRIPTION "This project is meant to create "
#define DESCRIPTION2 "an ant farm manager."
#define TITLE "                 LEM-IN                                        "
#define INFO_T3 "Made by soleksiu.student.unit.ua in "
#define INFO_T4 "Unit Factory. Kyiv. Ukraine. 2018.\n"

static void	ft_get_ways_limit(t_lemin *data, int argc, char **argv, int i)
{
	int	value;

	value = 0;
	if (i + 1 == argc)
		ft_error(data, "incorrect ways limit: no value");
	if (ft_strnequ(argv[i + 1], "all", 3))
		value = 42;
	else if (!ft_isnumeric(argv[i + 1]) || argv[i + 1][0] == '-' ||
		(value = ft_atoi(argv[i + 1])) == 0)
		ft_error(data, "incorrect ways limit");
	data->flag_wl = value;
}

static int	ft_check_flag_name(t_lemin *data, int argc, char **argv, int *i)
{
	if (ft_strnequ(argv[*i], "-h", 3) || ft_strnequ(argv[*i], "--help", 8))
		data->flag_h = *i;
	else if (ft_strnequ(argv[*i], "-i", 3) || ft_strnequ(argv[*i], "--info", 7))
		data->flag_i = *i;
	else if (ft_strnequ(argv[*i], "-s", 3) || ft_strnequ(argv[*i], "--sort", 7))
		data->flag_s = *i;
	else if (ft_strnequ(argv[*i], "-q", 3) || ft_strequ(argv[*i], "--quiet"))
		data->flag_q = *i;
	else if (ft_strnequ(argv[*i], "-m", 3) || ft_strequ(argv[*i], "--map_info"))
		data->flag_m = (*i);
	else if (!data->flag_p && (ft_strnequ(argv[*i], "-p", 3) ||
		ft_strnequ(argv[*i], "--paths", 8)))
		data->flag_p = (*i);
	else if (!data->flag_wl && (ft_strnequ(argv[*i], "-wl", 4) ||
		ft_strnequ(argv[*i], "--ways_limit", 12)) && (*i += 1))
		ft_get_ways_limit(data, argc, argv, (*i) - 1);
	else if (!data->flag_wall && (ft_strnequ(argv[*i], "-Wall", 7) ||
		ft_strnequ(argv[*i], "--warn_all", 11)))
		data->flag_wall = (*i);
	else if (!data->flag_e && (ft_strnequ(argv[*i], "-e", 3) ||
		ft_strnequ(argv[*i], "--errors_extended", 18)))
		data->flag_e = (*i);
	else
		return (0);
	return (1);
}

static int	ft_args_parsing(t_lemin *data, int argc, char **argv)
{
	int	i;

	i = 0;
	while (++i < argc)
	{
		if (!ft_check_flag_name(data, argc, argv, &i))
			return (0);
	}
	return (1);
}

/*
** Flags will be parsed and checked here.
*/

void		ft_flags_manager(t_lemin *data, int argc, char **argv)
{
	if (!(ft_args_parsing(data, argc, argv)))
	{
		ft_error(data, "usage");
		ft_printf("{underline}use flag -h or --help form more info.{eoc}\n");
		exit(1);
	}
	if (data->flag_i || data->flag_h)
	{
		if (data->flag_i)
		{
			ft_printf("                       {bold}%s\n{eoc}", TITLE);
			ft_printf("%50s%s\n", DESCRIPTION, DESCRIPTION2);
			write(1, ft_strjoin(INFO_T, INFO_T2), 89);
			ft_printf("%45s%s\n", INFO_T3, INFO_T4);
			exit(0);
		}
		if (data->flag_h)
			ft_display_help(data);
	}
}
