/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reset_vertices_params.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 19:51:44 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/12 19:51:46 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** Function used in finding ways algo.
** Called from ft_find_ways to reset vertices params
** and start over the process of search the new way.
*/

void	ft_reset_vertices_params(t_lemin *data)
{
	size_t		i;
	t_vertex	**graph;

	i = 0;
	graph = data->graph;
	while (graph[i])
	{
		if (graph[i]->in_way == 0)
		{
			graph[i]->visited = 0;
			graph[i]->from = NULL;
		}
		i++;
	}
	data->start->visited = 0;
	data->start->from = NULL;
}
