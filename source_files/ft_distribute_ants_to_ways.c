/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_distribute_ants_to_ways.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 12:56:01 by soleksiu          #+#    #+#             */
/*   Updated: 2018/11/23 12:56:03 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** Function iterates over all ways and putting the ants to them;
** the ants will be added to vertises in ft_move_ants;
*/

void		ft_distribute_ants_to_ways(t_lemin *data, int *cur_ant_nb)
{
	size_t	i;
	t_way	*cur_way;

	i = 0;
	while (data->ways[i] && data->ways[i]->way)
	{
		cur_way = data->ways[i];
		if (*cur_ant_nb > data->lem_nb)
			cur_way->ant_to_add = -1;
		else if ((data->lem_nb - *cur_ant_nb >=
		cur_way->delta) && (cur_way->in_use = 1))
		{
			cur_way->ant_to_add = *cur_ant_nb;
			if (*cur_ant_nb == data->lem_nb)
			{
				*cur_ant_nb = -1;
				return ;
			}
			else
				(*cur_ant_nb)++;
		}
		i++;
	}
}
