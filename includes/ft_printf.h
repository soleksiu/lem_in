/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 18:20:23 by soleksiu          #+#    #+#             */
/*   Updated: 2018/02/04 21:59:02 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <time.h>
# include <limits.h>
# include <stddef.h>
# include <sys/types.h>

# define CONVERS "sSpdDioOuUxXcCbrkFfGgbnmeEwkTaA"
# define NB_DIGITS "0123456789abcdef0123456789ABCDEF"
# define UNS "OoUuXx"
# define BASE "OoXxb"
# define SIGN "diDb"
# define FLT_SP "fFgGaAeE"
# define TEXT "sScCrp"
# define LENMOD "lhjzLtq"
# define FLAGS "+- #0'$"

# define EOC "\x1B[0m"
# define RESET "\x1B[0m"
# define RED "\x1B[31m"
# define GREEN "\x1B[32m"
# define YELLOW "\x1B[33m"
# define BLUE "\x1B[34m"
# define MAGENTA "\x1B[35m"
# define CYAN "\x1B[36m"
# define WHITE "\x1B[37m"
# define BOLD "\033[1m"
# define ITALIC "\033[3m"
# define UNDERLINE "\033[4m"

typedef struct	s_printf
{
	int			flplus;
	int			flspace;
	int			flsharp;
	int			flzero;
	int			flminus;
	int			fldl;
	int			flst;
	int			flquote;
	int			wd;
	int			prec;
	int			len_modif;
	int			nbr_len;
	char		sp;
}				t_printf;

extern t_printf	*g_data;
extern char		*g_fmt;
extern int		g_ret;
extern va_list	g_ap;
extern va_list	g_dol;
extern int		g_arg_count;
extern int		g_fd;

int				ft_printf(const char *format, ...);
int				ft_pf_parse_specificator(void);
void			ft_pf_parse_wildcard(char *pt, int flpr);
int				ft_pf_dispatcher(void);
int				ft_pf_nbr_len(long long int nbr);
int				ft_pf_nbr_len_unsigned(unsigned long long int nbr, int base);
void			ft_pf_nbr_len_float(long double fnb);
void			ft_pf_putnbr_base(long long int nb, int base);
void			ft_pf_putnbr_base_unsigned(long long unsigned int nb, int base);
void			ft_pf_print_pointer(void *addr);
int				ft_pf_putunicode(wchar_t c);
int				ft_pf_putstr_unicode(wchar_t *str);
void			ft_pf_putfloat(long double nb);
void			ft_pf_putfloat_max(double k);
void			ft_pf_putfloat_exp(long double fnb);
void			ft_pf_putfloat_g(long double fnb);
void			ft_pf_putstr_non_printable(char *str);
void			ft_pf_width_usage(const char *s);
int				ft_pf_prec_usage(long long nb);
void			ft_pf_flsharp(void);
size_t			ft_pf_strlen_unicode(wchar_t *s);
int				ft_pf_null_case_nb(int base);
void			ft_pf_data_postprocessing(void);
int				ft_pf_flqoute(int *i);
int				ft_pf_find_exp_value(long double fnb);
void			ft_pf_print_memory(void *addr, long long size);
char			*ft_pf_strnstr(const char *hayst, const char *ndl, size_t len);
void			ft_pf_manage_bracket(void);
void			ft_pf_puttime(void);
void			ft_pf_putfloat_hex(double nb);
int				ft_pf_putfloat_hex_calc(int *k, int *dg_v, long nb, double fl);
void			ft_pf_modify_len(long long *nb);
int				ft_pf_check_lim_fl(double fnb);
void			ft_pf_check_flquote(long long int nb, int base);
void			ft_pf_width_usage_float(long double nb);
void			ft_pf_width_usage_float_exp(double nb, int exp);
int				ft_pf_check_g(int next, long double nb);
void			ft_pf_infin_add(int *i, int *j, char *s1, char *s2);

int				ft_pf_atoi(const char *str);
size_t			ft_pf_strlen(const char *s);
void			*ft_pf_memset(void *b, int c, size_t len);
void			ft_pf_putchar(char c);
void			ft_pf_putchar_fd(char c, int fd);
void			ft_pf_putstr(const char *s);
char			*ft_pf_strchr(const char *s, int c);
void			*ft_pf_memcpy(void *dst, const void *src, size_t n);

#endif
