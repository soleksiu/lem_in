/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 13:57:11 by soleksiu          #+#    #+#             */
/*   Updated: 2018/12/17 18:33:03 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H
# include "ft_printf.h"
# include "libft.h"

typedef struct		s_vect
{
	double			x;
	double			y;
	double			z;

}					t_vect;

typedef struct		s_vertex
{
	const char		*name;
	t_vect			coords;
	size_t			ants_inside;
	t_list			*adj_lst;
	size_t			degree;
	int				visited;
	void			*from;
	size_t			in_way;
	int				lem_inside;
}					t_vertex;

typedef	struct		s_way
{
	t_vertex		**way;
	size_t			len;
	size_t			ind_of_head;
	int				in_use;
	int				ant_to_add;
	int				delta;
}					t_way;

typedef struct		s_lemin
{
	char			*line;
	t_list			*lst;
	int				flag_e;
	int				flag_i;
	int				flag_h;
	int				flag_m;
	int				flag_p;
	int				flag_q;
	int				flag_s;
	int				flag_wl;
	int				flag_wall;
	int				lem_nb;
	size_t			vertex_nb;
	size_t			edge_nb;
	t_vertex		**graph;
	t_vertex		*start;
	t_vertex		*end;
	t_vertex		**queue;
	t_way			**ways;
	int				ways_available;
	size_t			ways_found;
	int				all_ways_found;
	int				short_way;
	size_t			min_way_len;
	int				inp_e;
	int				start_ind;
	int				end_ind;
	int				rooms_readed;
	size_t			cur_ln_ind;
	size_t			lem_nb_ind;
	size_t			start_cm_ind;
	size_t			end_cm_ind;
	size_t			lst_com_ind;
	t_list			*graph_lst;
	int				input_processing;
}					t_lemin;

void				ft_read_input(t_lemin *data);
void				ft_allocate_memory(t_lemin *data);
void				ft_parse_data(t_lemin *data, size_t *i);
t_vertex			*ft_parse_vertex(t_lemin *data, const char **arr);
void				ft_parse_edge(t_lemin *data, const char *str);
void				ft_parse_command(t_lemin *data, int i);
void				ft_check_graph_for_errors(t_lemin *data);
void				ft_find_ways(t_lemin *data);
void				ft_find_path_bfs(t_lemin *data);
void				ft_get_way(t_lemin *data);
void				ft_reset_vertices_params(t_lemin *data);
void				ft_error(t_lemin *data, char *str);
void				ft_ants_move_and_output(t_lemin *data);
void				ft_distribute_ants_to_ways(t_lemin *data, int *cur_ant_nb);
void				ft_move_ants(t_lemin *data, size_t way_ind);
void				ft_output_ants_pos(t_lemin *data, t_way **ways);
void				ft_display_help(t_lemin *data);
void				ft_flags_manager(t_lemin *data, int argc, char **argv);
void				ft_print_graph_params(t_lemin *data);
void				ft_print_ways(t_lemin *data);
void				ft_output_map(t_lemin *data);
#endif
