#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/08 16:02:30 by soleksiu          #+#    #+#              #
#    Updated: 2018/07/08 16:02:35 by soleksiu         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

SRCDIR = source_files

OBJDIR = object_files

SRC = main.c ft_display_help.c ft_flags_manager.c  ft_error.c \
	  ft_read_input.c ft_allocate_memory.c\
	  ft_parse_data.c ft_parse_vertex.c ft_parse_edge.c ft_parse_command.c \
	  ft_check_graph_for_errors.c \
	  ft_find_ways.c ft_find_path_bfs.c ft_get_way.c \
	  ft_reset_vertices_params.c  \
	  ft_print_graph_params.c ft_print_ways.c \
	  ft_output_map.c ft_ants_move_and_output.c  ft_distribute_ants_to_ways.c \
	  ft_move_ants.c ft_output_ants_pos.c
	  
OBJ = $(SRC:.c=.o)
OBJS = $(addprefix $(OBJDIR)/,$(SRC:.c=.o))

NAME = lem-in

WRN =  -Wall -Wextra -Werror

all: objdir $(NAME)

$(NAME): objdir lib $(OBJS)
	@gcc $(WRN) $(OBJS) -L ./libs/libft -lft -L ./libs/ft_printf/ -lftprintf  -L /usr/local/lib/ -lmlx -framework OpenGL -framework AppKit -o $(NAME) 

lib:
	@make -C ./libs/libft/

objdir: 
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	@gcc $(WRN) -I includes -I /usr/local/include -c $< -o $@  


backup: fclean
	@mkdir -p ~/Desktop/backup_$(NAME)
	@cp -r * ~/Desktop/backup_$(NAME)

fbackup: backup ext_backup

test: all
	@./$(NAME) < maps/7_ways

clean:
	@rm -rf $(OBJ)
	@make clean -C ./libs/libft

fclean: clean
	@find . -name "*~" -delete -or -name "#*#" -delete -or -name ".DS_Store" -delete -or -name "a.out" -delete
	@make fclean -C ./libs/libft/
	@rm -rf $(NAME)
	@rm -rf $(OBJDIR)

re: fclean all