# Lem-in - Algorithmic project.
### Language of development: C.
This is a learning project of Unit Factory coding school, which using
'ecole 42' franchise and same learning programm as the network of innovative
schools across the world in such countries as USA, France, Netherlands.

 This project is meant to create an ant farm manager using BFS algorythm.
 
 To make life funnier, using the external libraries or frameworks was
forbidden or very limited for student projects. Libft included in repository
was developed by author as another lerning project in school 42. Available
external functions for this project is: open, read, write, close, malloc, free,
perror, strerror and exit. All additional stuff needed for project realisation
student must done by yourself.
 
 NOTE: Project was developed and tested on macOS High Sierra 10.13.3.
Unfortunately I have no ability to test it on other platforms for now.

## Installation
Use make to compile the project.

```make lem-in```
## Usage
Programm can get input from cin or file. You can use files from 'maps' repository
or create your own.

Execute with: 
```./lem-in```
or
```./avm tests < filename```
## Interface screenshots

### Standart programm output
![Screenshot](screenshots/output_normal.png)
### Help realisation
![Screenshot](screenshots/help.png)
### Flag -m output
![Screenshot](screenshots/flag_m.png)
### Flag -p output
![Screenshot](screenshots/flag_p.png)
### Errors handling
![Screenshot](screenshots/errors.png)
### Visualising graph structure
Execute with: 
```./visu-hex < filename```
![Screenshot](screenshots/visualiser1.png)
![Screenshot](screenshots/visualiser2.png)
